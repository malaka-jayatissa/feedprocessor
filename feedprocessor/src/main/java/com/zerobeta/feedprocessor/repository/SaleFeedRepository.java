package com.zerobeta.feedprocessor.repository;


import com.zerobeta.feedprocessor.model.SaleFeedDB;
import org.springframework.data.repository.CrudRepository;

public interface SaleFeedRepository extends CrudRepository<SaleFeedDB,Integer> {
    
}

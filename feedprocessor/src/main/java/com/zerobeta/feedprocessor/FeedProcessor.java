package com.zerobeta.feedprocessor;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Hello world!
 *
 */
@SpringBootApplication
public class FeedProcessor 
{
    public static void main( String[] args )
    {
        SpringApplication.run(FeedProcessor.class, args);
    }
}

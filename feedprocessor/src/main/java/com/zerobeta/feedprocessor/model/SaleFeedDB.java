package com.zerobeta.feedprocessor.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import com.zerobeta.common.model.SaleFeed;

@Entity
public class SaleFeedDB {
    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private Integer id;
    private String productCode;
    private String posTransactionTime;
    private float posTransactionAmount;
    private float posDiscount;
    private String feedReceiveTime;
    private String businessID;
    private long sequenceNumber;
    private long kafkaOffset;
    private String feedDecodeTime;


    public String getProductCode() {
        return productCode;
    }

    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }

    public String getPosTransactionTime() {
        return posTransactionTime;
    }

    public void setPosTransactionTime(String posTransactionTime) {
        this.posTransactionTime = posTransactionTime;
    }

    public float getPosTransactionAmount() {
        return posTransactionAmount;
    }

    public void setPosTransactionAmount(float posTransactionAmount) {
        this.posTransactionAmount = posTransactionAmount;
    }

    public float getPosDiscount() {
        return posDiscount;
    }

    public void setPosDiscount(float posDiscount) {
        this.posDiscount = posDiscount;
    }

    public String getFeedReceiveTime() {
        return feedReceiveTime;
    }

    public void setFeedReceiveTime(String feedReceiveTime) {
        this.feedReceiveTime = feedReceiveTime;
    }

    public String getBusinessID() {
        return businessID;
    }

    public void setBusinessID(String businessID) {
        this.businessID = businessID;
    }

    public long getSequenceNumber() {
        return sequenceNumber;
    }

    public void setSequenceNumber(long sequenceNumber) {
        this.sequenceNumber = sequenceNumber;
    }

    public long getKafkaOffset() {
        return kafkaOffset;
    }

    public void setKafkaOffset(long kafkaOffset) {
        this.kafkaOffset = kafkaOffset;
    }

    public String getFeedDecodeTime() {
        return feedDecodeTime;
    }

    public void setFeedDecodeTime(String feedDecodeTime) {
        this.feedDecodeTime = feedDecodeTime;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "SaleFeed [businessID=" + businessID + ", feedDecodeTime=" + feedDecodeTime + ", feedReceiveTime="
                + feedReceiveTime + ", kafkaOffset=" + kafkaOffset + ", posDiscount=" + posDiscount
                + ", posTransactionAmount=" + posTransactionAmount + ", posTransactionTime=" + posTransactionTime
                + ", productCode=" + productCode + ", sequenceNumber=" + sequenceNumber + "]";
    }

}

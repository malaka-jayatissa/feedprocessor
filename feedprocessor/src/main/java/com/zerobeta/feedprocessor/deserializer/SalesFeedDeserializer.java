package com.zerobeta.feedprocessor.deserializer;

import java.io.ByteArrayInputStream;
import java.io.ObjectInputStream;
import java.util.Map;
import com.zerobeta.common.model.SaleFeed;
import org.apache.kafka.common.serialization.Deserializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SalesFeedDeserializer implements Deserializer<SaleFeed> {

    private static final Logger logger = LoggerFactory.getLogger(SalesFeedDeserializer.class);
    @Override
    public void configure(Map<String, ?> configs, boolean isKey) {
    }

    @Override
    public SaleFeed deserialize(String topic, byte[] data) {
    
        try
        {
            ByteArrayInputStream bis = new ByteArrayInputStream(data);
            ObjectInputStream ois = new ObjectInputStream(bis);
            return (SaleFeed)ois.readObject();
        }
        catch (Exception e)
        {
            logger.error("Error in deserializing " + e.getMessage());
            return null;
        }
    }

    @Override
    public void close() {
    }
    
}

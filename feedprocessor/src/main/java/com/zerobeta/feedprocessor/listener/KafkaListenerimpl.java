package com.zerobeta.feedprocessor.listener;

import java.util.Map;

import com.zerobeta.common.model.SaleFeed;
import com.zerobeta.feedprocessor.model.SaleFeedDB;
import com.zerobeta.feedprocessor.repository.SaleFeedRepository;

import org.apache.kafka.common.TopicPartition;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.listener.ConsumerSeekAware;
import org.springframework.kafka.listener.ConsumerSeekAware.ConsumerSeekCallback;
import org.springframework.kafka.support.Acknowledgment;
import org.springframework.messaging.handler.annotation.Header;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Service;

import ch.qos.logback.core.joran.util.beans.BeanUtil;

import org.springframework.kafka.support.KafkaHeaders;

@Service
public class KafkaListenerimpl  implements ConsumerSeekAware {

    @Autowired
    private SaleFeedRepository salesRepository;

    private static final Logger logger = LoggerFactory.getLogger(KafkaListenerimpl.class);
    
    @KafkaListener(
        topics = "sale_feed",
        groupId = "${kafka.groupid}")
public void handleMessage(@Payload SaleFeed feed,
                               @Header(KafkaHeaders.RECEIVED_PARTITION_ID) Integer partition,
                               @Header(KafkaHeaders.OFFSET) Long offset) {

    feed.setKafkaOffset(offset);
    logger.info(feed.toString());
    SaleFeedDB dbObject = new SaleFeedDB();
    BeanUtils.copyProperties(feed, dbObject);
    logger.info(dbObject.toString());
    salesRepository.save(dbObject);
    
   
}

@Override
    public void registerSeekCallback(ConsumerSeekCallback callback) {
        // register custom callback
    }

    @Override
    public void onPartitionsAssigned(Map<TopicPartition, Long> assignments, ConsumerSeekCallback callback) {
    
        logger.info("onPartitionsAssigned");
        for(TopicPartition partition : assignments.keySet()){
             callback.seek("sale_feed", partition.partition(), 0);
        }
    }

    @Override
    public void onIdleContainer(Map<TopicPartition, Long> assignments, ConsumerSeekCallback callback) {
  		// executes when the Kafka container is idle
    }
    
}

package com.zerobeta.feedprocessor.configuration;

import java.util.HashMap;
import java.util.Map;

import com.zerobeta.common.model.SaleFeed;
import com.zerobeta.feedprocessor.deserializer.SalesFeedDeserializer;

import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.annotation.EnableKafka;
import org.springframework.kafka.config.ConcurrentKafkaListenerContainerFactory;
import org.springframework.kafka.core.ConsumerFactory;
import org.springframework.kafka.core.DefaultKafkaConsumerFactory;

@EnableKafka
@Configuration
public class KafkaConsumerConfiguration {

    @Value("${kafka.address}")
    private String kafkaAddress;

    @Value("${kafka.groupid}")
    private String groupID;

    @Bean
    public ConsumerFactory<String, SaleFeed> consumerFactory() {
        Map<String, Object> props = new HashMap<>();
        props.put(
                ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG,
                kafkaAddress);
        props.put(
                ConsumerConfig.GROUP_ID_CONFIG,
                groupID);
        props.put(
                ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG,
                StringDeserializer.class);
        props.put(
                ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG,
                SalesFeedDeserializer.class);
        return new DefaultKafkaConsumerFactory<>(props);
    }

    @Bean
    public ConcurrentKafkaListenerContainerFactory<String, SaleFeed> kafkaListenerContainerFactory() {

        ConcurrentKafkaListenerContainerFactory<String, SaleFeed> factory = new ConcurrentKafkaListenerContainerFactory<>();
        factory.setConsumerFactory(consumerFactory());
        return factory;
    }

}
